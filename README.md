## Getting the script running
1. Install python 3.9
2. pip install poetry
3. In the cloned folder:
    1. poetry install
    2. poetry shell
4. Run the script: python ./main.py
