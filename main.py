import concurrent.futures
import json

import progressbar
import requests
import urllib3
import mariadb
from config import Config

# Setup
config_file = Config()
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)  # Disable SSL warnings
items_per_run = int(config_file.get_config('general', 'items_per_run'))
num_of_uploading_workers = config_file.get_config('general', 'http_workers')
elastic_host = config_file.get_config('elasticsearch', 'host')
elastic_port = config_file.get_config('elasticsearch', 'port')
elastic_index = config_file.get_config('elasticsearch', 'index')
elastic_username = config_file.get_config('elasticsearch', 'username')
elastic_password = config_file.get_config('elasticsearch', 'password')
elastic_path = f'https://{elastic_host}:{elastic_port}/{elastic_index}/_bulk'
elastic_bulk = config_file.get_config('general', 'http_request_items_per_run')


# Functions
def connect_to_mariadb():
    conn = mariadb.connect(
        user=config_file.get_config('database', 'username'),
        password=config_file.get_config('database', 'password'),
        host=config_file.get_config('database', 'host'),
        database=config_file.get_config('database', 'db_name'),
        port=int(config_file.get_config('database', 'port'))
    )
    return conn.cursor(dictionary=True)


def convert_datetime_to_string(datetime):
    return datetime.strftime('%Y;%m;%d;T%H;%M;%S') if datetime else None


def convert_date_to_string(date):
    return date.strftime('%Y;%m;%d') if date else None


def clean_elastic_data(one_data_for_elastic):
    one_data_for_elastic['measuredOn'] = convert_date_to_string(one_data_for_elastic['measuredOn'])
    one_data_for_elastic['processing_completed_at'] = convert_datetime_to_string(
        one_data_for_elastic['processing_completed_at'])
    one_data_for_elastic['processing_failed_at'] = convert_datetime_to_string(
        one_data_for_elastic['processing_failed_at'])
    one_data_for_elastic['projectIds'] = [int(project_id) for project_id in
                                          one_data_for_elastic['projectIds'].split(',')]
    return one_data_for_elastic


def send_request_to_elastic(data_to_upload):
    elastic_response = requests.post(elastic_path,
                                     headers={'Content-type': 'application/json'},
                                     data=data_to_upload,
                                     verify=False,
                                     auth=(elastic_username, elastic_password)).text
    return elastic_response


def send_data_to_elastic(offset):
    print('Beggining query with offset: ', offset)
    while 1:
        try:
            cur = connect_to_mariadb()
            cur.execute(
                f"""
                SELECT serp_domains.*, GROUP_CONCAT(projectId) AS projectIds
                FROM serp_domains 
                JOIN serp_domains2projects ON domain = serp_domains.id GROUP BY domain 
                LIMIT {items_per_run} OFFSET {offset}
                """
            )
            break
        except mariadb.Error as e:
            print(f'Error connecting to MariaDB Platform: {e}, retrying')

    print('Query done - preparing data')
    data_to_upload = []
    for one_data_for_elastic in cur:
        data_to_upload.append(json.dumps({"index": {}}))  # Necessary for bulk insert
        data_to_upload.append(json.dumps(clean_elastic_data(one_data_for_elastic)))

    print(f'Sending out data using {num_of_uploading_workers} workers')
    with concurrent.futures.ThreadPoolExecutor(max_workers=num_of_uploading_workers) as executor:
        return_urls = {executor.submit(send_request_to_elastic, '\n'.join(data_to_upload[a:a + elastic_bulk]) + '\n'): a for a in range(0, len(data_to_upload), elastic_bulk)}
        success_count = 0
        for future in concurrent.futures.as_completed(return_urls):
            try:
                print(future.result())
            except Exception as exc:
                print('Unsuccessful', exc)
            else:
                success_count += 1
                print(f'Successful {success_count} / {len(return_urls)}')


def write_progress_to_file(offset):
    with open('progress.txt', 'w') as progress_file:
        progress_file.write(str(offset))


print('Counting number of rows in table...')
cur = connect_to_mariadb()
cur.execute('SELECT count(id) FROM serp_domains')
num_of_rows = cur.fetchone()['count(id)']
print(f'Amount of rows: {num_of_rows}')

with open('progress.txt', 'r') as progress_file:
    if line := progress_file.read():
        offset = int(line)
        print('Loaded offset from progress.txt')
    else:
        offset = 0

for i in progressbar.progressbar(range(offset, num_of_rows, items_per_run), redirect_stdout=True):
    print(f'Starting iteration {i} to {i + items_per_run}')
    send_data_to_elastic(i)
    write_progress_to_file(i + items_per_run)
