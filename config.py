import configparser
from configparser import ConfigParser


class Config:
    def __init__(self):
        self.config = ConfigParser()
        self.config.read('config.ini')
        try:
            self._set_config_defaults()

            with open('config.ini', 'w') as f:
                self.config.write(f)
        except configparser.DuplicateSectionError as e:
            print('## Skipping config file creation ##')


    def _set_config_defaults(self):
        self.config.add_section('general')
        self.config.set('general', 'items_per_run', '10000')
        self.config.set('general', 'http_request_items_per_run', '2048')
        self.config.set('general', 'http_workers', '20')
        self.config.add_section('database')
        self.config.set('database', 'host', '127.0.0.1')
        self.config.set('database', 'port', '3306')
        self.config.set('database', 'username', 'root')
        self.config.set('database', 'password', 'root')
        self.config.set('database', 'db_name', 'collabim_serpdata')
        self.config.add_section('elasticsearch')
        self.config.set('elasticsearch', 'host', '127.0.0.1')
        self.config.set('elasticsearch', 'port', '5601')
        self.config.set('elasticsearch', 'username', 'root')
        self.config.set('elasticsearch', 'password', 'root')
        self.config.set('elasticsearch', 'index', 'serpdata')

    def get_config(self, section, key):
        return self.config.get(section, key)

